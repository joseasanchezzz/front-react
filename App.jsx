import React from "react";
import { BrowserRouter as Router, browserHistory, Route, Switch } from "react-router-dom";
import Home from "./src/views/home/Home";
import Login from './src/views/login/Login';
import Register from './src/views/register/Register';
// import FormCreate from "./components/posts/FormCreate";
// import FormEdit from "./components/posts/FormEdit";
// import PostDetail from "./views/postDetail/PostDetail";
// import NoMatch from "./views/noMatch/NoMatch";
// import "./App.css";

function App() {
  return (
    <Router history={browserHistory}>
      <Switch>
        <Route path="/" exact component={Login} />
      <Route path="/user/new" component={Register} />
        <Route path="/home/:token"  component={Home} />
        {/*   <Route path="/post/detail/:id" exact component={PostDetail} />
        <Route component={NoMatch} /> */}
      </Switch>
    </Router>
  );
}

export default App;