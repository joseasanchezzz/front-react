import axios from 'axios'
import "regenerator-runtime/runtime.js";


export const login= async  (data)=>{
 let user = null;
    const headers = {
        'Content-Type': 'application/json; charset=utf-8',
      }

  await  axios.post('http://localhost:9090/login', data,{
        headers: headers
    })
    .then(result=>{
        user = result.data;
    }).catch(err=>{
        console.log('====================================')
        console.log('error', err)
        console.log('====================================')
    })

    return user;
}




export const getImg = async(token) => {
let data = '';
    await axios.get(`http://localhost:9090/home/${token}`)
    .then( result => {
            data = result.data.data;
    })
    .catch(err => {
        console.log(err);
    })
    console.log(data);
    return data;
}



export const save = async(data) =>{
   await axios.post('http://localhost:9090/users', data)
   .then(result =>{
   })
   .catch(err => {
       console.log(err);
   })
}