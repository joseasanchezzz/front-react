import React,{useEffect,useState} from 'react';
import {getImg} from '../../services/services';

const Home = (props) => {

    const [data, setData] = useState(null);

useEffect(()=>{
    const { token } = props.match.params;

    const get = async(token)=>{
        const info = await getImg(token);
        if(info){

            setData(info)
        }else{
            props.history.push('/');
        }
}
get(token);

setInterval( ()=>{
    get(token);
},120000);


},[])




    return (
    <div className="container">
        <div>
           {data ?  <div className="text-center mb-4"><h1 >Listado</h1></div> : null}
        </div>
        <div className="row">
        { data ?  
        data.map( info => {

            return(
                <div className="col-md-6">
            <ul className="list-unstyled">
            <li className="media">
            <img src={info.image} className="mr-3" alt={info.name} height="120" width="120" />
                <div className="media-body">
                <h5 className="mt-0 mb-1">{info.name}</h5>
                <p className="mt-0 mb-1"><b>Estado:</b>  {info.status}</p>
                <p className="mt-0 mb-1"><b>Género: </b> {info.gender}</p>
                <p className="mt-0 mb-1"><b>Especie:</b> {info.species}</p>
                </div>
            </li>
            </ul>
            </div>
           )
        })
       

        : <h1>Cargando...</h1>
     
}
</div>
    </div>
    
    )
}

export default Home
