import React from 'react';
import { Button, Form, Label, Input, Col,Row } from 'reactstrap';
import {Link, Router} from 'react-router-dom';
import {save} from '../../services/services';

const Register = (props) => {

    const handleSubmit = e => {
        e.preventDefault();
  const data = {
      email: e.target.email.value,
      password: e.target.password.value
  }
        save(data);
    props.history.push('/');
    }

    return (
        <div className="container">
        <div  className="row justify-content-center align-items-center">
            <div  className="col-md-6">
                <div id="login-box" className="col-md-12">
                    <Form  className="form"  onSubmit={handleSubmit}>
                        <h3 className="text-center text-info">Regitrar</h3>
                        <div className="form-group">
                            <Label for="username" className="text-info">Email:</Label><br/>
                            <Input type="text" name="email"  className="form-control"/>
                        </div>
                        <div className="form-group">
                            <Label for="password" className="text-info">Password:</Label><br/>
                            <Input type="text" name="password"  className="form-control"/>
                        </div>
                        <Row>
                        <Col md={6}>
                        <div className="form-group">
                        <Button type='submit' color="primary">Registrar</Button>
                        </div>
                        </Col>
                        <Col md={6}>
                        <div className="text-right">
                                    <Link to={'/'}>
                                            <Button color="danger">  Cancelar</Button>
                                   </Link>
                        </div>
                        </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Register

