import React from 'react';
import { Button, Form, Label, Input, Col,Row } from 'reactstrap';
import {Link} from 'react-router-dom';
import {login} from '../../services/services';
import "regenerator-runtime/runtime.js";

const Login = (props) => {

    const handleSubmit = e => {
        e.preventDefault();
        const data = {
          email: e.target.email.value,
          password: e.target.password.value,
        }

        const loginSend = async(data) =>{
          let user =  await login(data);
          if(user){
          props.history.push(`/home/${user.token}`)
          }
        }
        loginSend(data);
     
    }



  return (
     <div>    
            <div className="container mt-3">
                <div  className="row justify-content-center align-items-center">
                    <div  className="col-md-6">
                        <div id="login-box" className="col-md-12">
                            <Form  className="form"  onSubmit={handleSubmit}>
                                <h3 className="text-center text-info">Login</h3>
                                <div className="form-group">
                                    <Label for="username" className="text-info">Email:</Label><br/>
                                    <Input type="text" name="email"  className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <Label for="password" className="text-info">Password:</Label><br/>
                                    <Input type="text" name="password"  className="form-control"/>
                                </div>
                                <Row>
                                <Col md={6}>
                                <div className="form-group">
                                <Button type='submit' color="primary">Iniciar sesión</Button>
                                </div>
                                </Col>
                                <Col md={6}>
                                <div className="text-right">
                                <Link to={'/user/new'}>
                              <Button className="btn btn-link" color="default"> Regístrese Aquí</Button>
                                </Link>
                                </div>
                                </Col>
                                </Row>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

  );
}

export default Login;